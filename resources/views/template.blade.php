<!DOCTYPE html>
<html lang="en" dir="ltr">

    @include('Komponen.header')

    @include('Komponen.navbar')

    @yield('Konten')

    @include('Komponen.footer')

</html>
